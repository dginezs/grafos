/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafos;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.lang.Object;
import java.util.Iterator;
import java.util.Objects;
import java.util.Random;
import org.jgrapht.Graph;
import org.jgrapht.generate.CompleteGraphGenerator;
import org.jgrapht.generate.GraphGenerator;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.io.GmlExporter;
import org.jgrapht.traverse.DepthFirstIterator;

public class Grafos {

public static class EmptyGraphGenerator {
    int n;
    double p;
    
    public  EmptyGraphGenerator(int n,double p){
        this.n=n;
        this.p=p;
    }
    
    public  Graph<Object, DefaultEdge> EmptyGraphGenerator(){
        
    FileWriter w;
       Graph<Object,DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);
        for (int i = 0; i < n; i++) {
            g.addVertex(i);
        }
 
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (Math.random() < p) {
                    g.addEdge(i, j);
                }
            }
        }
        try {
        
GmlExporter<Object, DefaultEdge> exporter = new GmlExporter<Object, DefaultEdge>();
w = new FileWriter("grafo.gml");
exporter.exportGraph(g,w);
} catch (IOException e) {
e.printStackTrace();
}
        return g;
    }
}
/**
 *
 * @author Diego Ginez
 */

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int nod=0, vert=0, opc;
        String res1;
        double p= .4;
       
       do{
           Scanner input = new Scanner(System.in);
       System.out.println("1. MODELO G(n,m) DE ERDOS Y RÉNYI");   
       System.out.println("2. MODELO G(n,p) DE GILBERT");
       System.out.println("3. Finalizar\n");
       System.out.println("Seleccione una opción");
       opc = input.nextInt();
       switch(opc){
            case 1:
                System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                System.out.println("1. Modelo Gn,m de Erdös y Rényi");
                String res;
                System.out.println("Indique el número de nodos");
                nod=input.nextInt();
                
         Graph<Object,DefaultEdge> g;
         g=new SimpleGraph<>(DefaultEdge.class);
         EmptyGraphGenerator genera;
         genera = new EmptyGraphGenerator(nod,p);
         
         g = genera.EmptyGraphGenerator();
         
         Iterator<Object> iterador = new DepthFirstIterator<>(g);
        Object v;
        while (iterador.hasNext()) {
            v = iterador.next();
            System.out.println(v.toString() + " -> " + g.edgesOf(v).toString());
        }
                System.out.println("¿Desea continuar?(n/y)");
                res1 = input.next();
                System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                break;
            case 2:
                System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                System.out.println("2. Modelo G(n,p) de Gilbert");
                System.out.println("Indique el número de nodos");
                nod=input.nextInt();
                double p2=Math.random()*1+0;
         genera = new EmptyGraphGenerator(nod,p2);
         
         g = genera.EmptyGraphGenerator();
        Iterator<Object> iterador2 = new DepthFirstIterator<>(g);
        while (iterador2.hasNext()) {
            v = iterador2.next();
            System.out.println(v.toString() + " -> " + g.edgesOf(v).toString());
        }
        System.out.println(p2);
                System.out.println("¿Desea continuar?(n/y)");
                res1 = input.next();
                System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                break;
            
            case 3:
                res1="n";
                break;
            default:
                System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
                System.out.println("Entrada inválida\n");
                res1="y";
                break;
                
        }
       }while(res1.equalsIgnoreCase("y"));
       System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("\tGRACIAS!");
      
}

}